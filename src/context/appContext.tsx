import React, { createContext, useContext, useEffect, useState } from 'react'
import { CategoriesType, EntriesType, initalEntries, initialCategories } from '../data/categories'

type AppType = {
  categories: CategoriesType[]
  entries: EntriesType[]
  addCategory: (c: CategoriesType) => void
  updateCategories: (c: CategoriesType[]) => void
  addEntry: (e: EntriesType) => void
  updateEntries: (e: EntriesType[]) => void
}

type ProviderType = { children: React.ReactNode }

export const AppContext = createContext({} as AppType)

export const AppProvider = ({ children }: ProviderType) => {
  const [categories, setCategories] = useState(initialCategories)
  const [entries, setEntries] = useState<EntriesType[]>(initalEntries)

  function updateCategories(data: CategoriesType[]) {
    setCategories(data)
    localStorage.setItem('categories', JSON.stringify(data))
  }
  function updateEntries(data: EntriesType[]) {
    setEntries(data)
    localStorage.setItem('entries', JSON.stringify(data))
  }

  useEffect(() => {
    categories.map(category => {
      entries.map(entry => {
        if (category.name === entry.category) {
        return  category.spendBuget = category.spendBuget! + entry.amount
        }
      })
    }
    )
  }, [categories || entries])

  useEffect(() => {
    const storage = localStorage.getItem('categories')

    if (storage === null) {
      setCategories(initialCategories)
      localStorage.setItem('categories', JSON.stringify(initialCategories))
    }
    if (storage !== null) {
      setCategories(JSON.parse(storage))
    }
  }, [])
  useEffect(() => {
    const storage = localStorage.getItem('entries')

    if (storage === null) {
      setEntries(initalEntries)
      localStorage.setItem('entries', JSON.stringify(initalEntries))
    }
    if (storage !== null) {
      setEntries(JSON.parse(storage))
    }
  }, [])

  const addCategory = (category: CategoriesType) => {
    const filteredCategories = categories.filter((item) => item.id !== category.id)
    setCategories([...filteredCategories, category])

    localStorage.setItem(
      'categories',
      JSON.stringify([...filteredCategories, category])
    )
  }
  const addEntry = (entry: EntriesType) => {
    const filteredEntries = entries.filter((item) => item.id !== entry.id)
    setEntries([...filteredEntries, entry])

    localStorage.setItem(
      'entries',
      JSON.stringify([...filteredEntries, entry])
    )
  }

  const value = { categories, entries, addCategory, addEntry, updateCategories, updateEntries }
  return <AppContext.Provider value={value}>{children}</AppContext.Provider>
}

export const useApp = () => useContext(AppContext)

