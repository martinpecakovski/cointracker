export type CategoryType = "income" | "expense";

export type CategoriesType = {
  id: number;
  name: string;
  type: CategoryType;
  icon: string;
  budget?: number;
  spendBuget?: number,
  isEnabled: boolean;
};
export type EntriesType = {
  id: number;
  type: CategoryType;
  category: string;
  amount: number;
  date: string,
  desc?: string;
};

export const initalEntries: EntriesType[] = [
  {
    id: Date.now(),
    type: 'expense',
    category: 'Bills',
    amount: 0,
    date: '16.09.2022'
  },
  {
    id: Date.now() + 5,
    type: 'income',
    category: 'Salary',
    amount: 0,
    date: '16.09.2022'
  },
]

export const initialCategories: CategoriesType[] = [
  {
    id: Date.now(),
    name: "Food",
    icon: "fastfood",
    type: "expense",
    budget: 10000,
    spendBuget: 0,
    isEnabled: true,
  },
  {
    id: Date.now() + 2,
    name: "Bills",
    icon: "attach_money",
    type: "expense",
    budget: 7000,
    spendBuget: 0,
    isEnabled: true,
  },
  {
    id: Date.now() + 3,
    name: "Salary",
    icon: "attach_money",
    type: "income",
    budget: 17000,
    spendBuget: 0,
    isEnabled: true,
  },
  {
    id: Date.now() + 4,
    name: "scholarship",
    icon: "school_icon",
    type: "expense",
    budget: 540,
    spendBuget: 0,
    isEnabled: true,
  },
  {
    id: Date.now() + 5,
    name: "Gym",
    icon: "fitness_center_icon",
    type: "expense",
    budget: 650,
    spendBuget: 0,
    isEnabled: true,
  },
  {
    id: Date.now() + 6,
    name: "Freelance income",
    icon: "attach_money",
    type: "income",
    budget: 35000,
    spendBuget: 0,
    isEnabled: true,
  },
  {
    id: Date.now() + 7,
    name: "Vacation",
    icon: "flight_icon",
    type: "expense",
    budget: 2500,
    spendBuget: 0,
    isEnabled: true,
  },
  {
    id: Date.now() + 8,
    name: "Medical check up",
    icon: "local_hospital_icon",
    type: "expense",
    budget: 700,
    spendBuget: 0,
    isEnabled: true,
  },
];


export const initialIcons = [
  {
    name: 'Phone',
    icon: 'add_ic_call'
  },
  {
    name: 'Money',
    icon: 'attach_money'
  },
  {
    name: 'Medic',
    icon: 'local_hospital_icon'
  },
  {
    name: 'Flight',
    icon: 'flight_icon'
  },
  {
    name: 'Fitness',
    icon: 'fitness_center_icon'
  },
  {
    name: 'School',
    icon: 'school_icon'
  },
  {
    name: 'Fastfood',
    icon: 'fastfood'
  },
]
