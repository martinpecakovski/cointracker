import { Button } from "@mui/material";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import { AppProvider } from "./context/appContext";
import Categories from "./pages/Categories";
import Overview from "./pages/Overview";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";
import Statistics from "./pages/Statistics";
import Welcome from "./pages/Welcome";
import WelcomeThree from "./pages/WelcomeThree";
import WelcomeTwo from "./pages/WelcomeTwo";


function App() {
  return (
    <AppProvider>
      <div className="App">
        <Routes>
          <Route path="/" element={<SignIn />} />
          <Route path="/singup" element={<SignUp />} />
          <Route path="/welcome" element={<Welcome />} />
          <Route path="/welcometwo" element={<WelcomeTwo />} />
          <Route path="/welcomethree" element={<WelcomeThree />} />
          <Route path="/overview" element={<Overview />} />
          <Route path="/categories" element={<Categories />} />
          <Route path="/statistics" element={<Statistics />} />
        </Routes>
      </div>
    </AppProvider>
  );
}

export default App;
