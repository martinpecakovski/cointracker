import { Backdrop, Box, Dialog, Grid, Paper, Typography } from '@mui/material';
import Icon from '@mui/material/Icon';
import LinearProgress from '@mui/material/LinearProgress';
import { useState } from 'react';
import AddEditEntry from '../components/AddEditEntry';
import Header from '../components/Header';
import Navigation from '../components/Navigation';
import { useApp } from '../context/appContext';
import { EntriesType } from '../data/categories';


export default function Overview() {
  const { categories } = useApp()
  const { addEntry } = useApp()
  const { entries } = useApp()

  const [addOpen, setAddOpen] = useState(false)
  const [updateOpen, setUpdateOpen] = useState(false)
  const data: EntriesType = {
    id: new Date().valueOf(),
    type: 'income',
    category: '',
    amount: 0,
    date: '',
    desc: '',
  }
  const [updateData, setUpdateData] = useState<EntriesType>({
    id: new Date().valueOf(),
    type: 'income',
    category: '',
    amount: 0,
    date: '',
    desc: '',
  })

  const handleAddClose = () => {
    setAddOpen(false)
  }
  const handleUpdateClose = () => {
    setUpdateOpen(false)
  }

  return (
    <Box>
      {/* header */}
      <Header heading='Overview' />
      {/* /header/ */}
      <Box sx={{ padding: '1.5rem', paddingBottom: "70px" }}>
        <Grid container item spacing={2}>
          <Grid item xs={12} md={4}>
            {/* // income */}
            <Paper elevation={8} sx={{ marginBottom: '2.3rem' }}>
              <Box sx={{ padding: '0.7rem', backgroundColor: '#F4F4F4', color: '#979797', textAlign: 'left' }}>
                <Typography>Income</Typography>
              </Box>

              {/* // */}
              {categories.map(item => (
                item.type === 'income' &&
                <Box key={`income-box-${item.id}`} sx={{ paddingTop: '0.5rem' }} className='backgoundHover'>
                  <Grid container spacing={2}>
                    <Grid item xs={2}>
                      <Icon>{item.icon}</Icon>
                    </Grid>
                    <Grid item xs={10}>
                      <Grid container spacing={2}>
                        <Grid item container xs={12}>
                          <Grid item xs={7} sx={{ textAlign: 'left' }}>
                            <Typography>{item.name}</Typography>
                          </Grid>
                          <Grid item xs={3}>
                            <Typography sx={{ textAlign: 'right' }}>{item.spendBuget}/{item.budget}</Typography>
                          </Grid>
                        </Grid>
                        <Grid item xs={11}>
                          <Box sx={{ width: "100%", marginBottom: '0.5rem', marginTop: '0' }}>
                            <LinearProgress variant="determinate" value={item.budget! / 100 * item.spendBuget!} color="secondary" />
                          </Box>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Box>

              )
              )}
            </Paper>
            {/* /income/ */}
          </Grid>
          <Grid item xs={12} md={4}>
            {/* // expenses */}
            <Paper elevation={8} sx={{ marginBottom: '2.3rem' }}>
              <Box sx={{ padding: '0.7rem', backgroundColor: '#F4F4F4', color: '#979797', textAlign: 'left' }}>
                <Typography>Expenses</Typography>
              </Box>
              {/* // */}
              {categories.map(item => (
                item.type === 'expense' &&
                <Box key={`epense-box-${item.id}`} className='backgoundHover' sx={{ paddingTop: '0.5rem' }}>
                  <Grid container spacing={2}>
                    <Grid item xs={2}>
                      <Icon>{item.icon}</Icon>
                    </Grid>
                    <Grid item xs={10}>
                      <Grid container spacing={2}>
                        <Grid item container xs={12}>
                          <Grid item xs={7} sx={{ textAlign: 'left' }}>
                            <Typography>{item.name}</Typography>
                          </Grid>
                          <Grid item xs={3}>
                            <Typography sx={{ textAlign: 'right' }}>{item.spendBuget}/{item.budget}</Typography>
                          </Grid>
                        </Grid>
                        <Grid item xs={11}>
                          <Box sx={{ width: "100%", marginBottom: '0.5rem', marginTop: '0' }}>
                            <LinearProgress variant="determinate" value={item.budget! / 100 * item.spendBuget!} color="secondary" />
                          </Box>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Box>
              ))}
            </Paper>
            {/* /expenses/ */}
          </Grid>
          <Grid item xs={12} md={4}>
            {/* // entries */}
            <Paper elevation={8} sx={{ marginBottom: '2.3rem' }}>
              <Box sx={{ padding: '0.7rem', backgroundColor: '#F4F4F4', color: '#979797', textAlign: 'left' }}>
                <Typography>Entries</Typography>
              </Box>

              {entries.map(entry => (
                <Box
                  onClick={() => {
                    setUpdateData({
                      id: entry.id,
                      type: entry.type,
                      category: entry.category,
                      amount: entry.amount,
                      date: entry.date,
                      desc: entry.desc
                    })
                    setUpdateOpen(true)
                  }}
                  key={`entry-box-${entry.id}`} className='backgoundHover' sx={{ paddingTop: '0.5rem', padding: ".8rem" }}>
                  <Grid container spacing={2}>
                    <Grid item xs={2}>
                      <Icon>monetization_on</Icon>
                    </Grid>
                    <Grid item xs={10} sx={{ borderBottom: '1px solid #979797' }}>
                      <Grid container spacing={2}>
                        <Grid item container xs={12}>
                          <Grid item xs={7} sx={{ textAlign: 'left' }}>
                            <Typography>{entry.category}</Typography>
                            <Typography sx={{ fontSize: '12px', color: '#979797' }}>{entry.date}</Typography>
                          </Grid>
                          <Grid item xs={3}>
                            <Typography sx={{ textAlign: 'right', color: entry.type === 'income' ? 'green' : 'red' }}>{entry.type === 'income' ? '+' : '-'}{entry.amount}</Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Box>
              ))}
              {/* // */}
            </Paper>
            {/* /entries/ */}
          </Grid>
        </Grid>
      </Box>



      <Backdrop
        sx={{
          color: '#fff',
          backgroundColor: 'rgba(255,255,255, .6)'
        }}
        open={addOpen}
      >
        <Dialog open={addOpen} onClose={handleAddClose} hideBackdrop={true}>
          <AddEditEntry
            buttonText='ADD'
            heading='Add New Entry'
            handleClose={handleAddClose}
            handleSuccess={handleAddClose}
            addUpdateEntry={addEntry}
            updateData={data}
          />
        </Dialog>
      </Backdrop>
      <Backdrop
        sx={{
          color: '#fff',
          backgroundColor: 'rgba(255,255,255, .6)'
        }}
        open={addOpen}
      >
        <Dialog open={updateOpen} onClose={handleUpdateClose} hideBackdrop={true}>
          <AddEditEntry
            buttonText='UPDATE'
            heading='Update Entry'
            handleClose={handleUpdateClose}
            handleSuccess={handleUpdateClose}
            addUpdateEntry={addEntry}
            updateData={updateData}
          />
        </Dialog>
      </Backdrop>

      {/* navigation */}
      <Navigation dialogOpen={() => setAddOpen(true)} />
      {/* /navigation/ */}
    </Box>
  )
}