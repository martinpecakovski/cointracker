import { Box, Grid, Paper, Typography } from '@mui/material';
import { Bar } from 'react-chartjs-2';
import Header from '../components/Header';
import Navigation from '../components/Navigation';
import 'chart.js/auto';
import { Chart } from 'react-chartjs-2';
import { useApp } from '../context/appContext';

type Props = {}



export default function Statistics({ }: Props) {

  const { categories } = useApp()
  const { entries } = useApp()

  const incomeOptions = {
    indexAxis: 'y' as const,
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    responsive: true,
  };
  const incomeCategories = categories.filter(category => category.type === 'income')
  const incomeLabels = incomeCategories.map(cat => cat.name);

  const incomeData = {
    labels: incomeLabels,
    datasets: [
      {
        label: 'Amount',
        data: incomeCategories.map(cat => cat.budget),
        borderColor: 'rgb(97, 249, 127)',
        backgroundColor: '#DFFFE6',
      },
    ],
  };
  const expensesOptions = {
    indexAxis: 'y' as const,
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    responsive: true,
  };

  const expensesCategories = categories.filter(category => category.type === 'expense')
  const expensesLabels = expensesCategories.map(cat => cat.name);

  const expensesData = {
    labels: expensesLabels,
    datasets: [
      {
        label: 'Amount',
        data: expensesCategories.map(cat => cat.budget),
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)'
      },
    ],
  };


  const entriesOptions = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
    },
  };

  const entriesLabels = entries.map(en => en.date);

  const incomeEntries = entries.filter(en => en.type === 'income')
  const incomeDataEntries = incomeEntries.map(item => item.amount)
  const expenseEntries = entries.filter(en => en.type === 'expense')
  const expenseDataEntries = expenseEntries.map(item => item.amount)





  const entriesData = {
    entriesLabels,
    datasets: [
      {
        label: 'Expenses',
        data: expenseDataEntries,
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)'
      },
      {
        label: 'Income',
        data: incomeDataEntries,
        borderColor: 'rgb(97, 249, 127)',
        backgroundColor: '#DFFFE6',
      },
    ],
  };


  return (
    <Box>
      <Header heading='Statistics' />
      <Box sx={{ padding: '1.5rem', paddingBottom: "70px" }}>
        <Grid container item spacing={2}>
          <Grid item xs={12} md={4}>
            {/* // income */}
            <Paper elevation={8} sx={{ marginBottom: '2.3rem' }}>
              <Box sx={{ padding: '0.7rem', backgroundColor: '#F4F4F4', color: '#979797', textAlign: 'left' }}>
                <Typography>Income</Typography>
              </Box>

              {/* // */}

              <Box sx={{ paddingTop: '0.5rem', maxWidth: '400px' }} className='backgoundHover'>
                <Chart type='bar' options={incomeOptions} data={incomeData} />
              </Box>



            </Paper>
            {/* /income/ */}
          </Grid>
          <Grid item xs={12} md={4}>
            {/* // expenses */}
            <Paper elevation={8} sx={{ marginBottom: '2.3rem' }}>
              <Box sx={{ padding: '0.7rem', backgroundColor: '#F4F4F4', color: '#979797', textAlign: 'left' }}>
                <Typography>Expenses</Typography>
              </Box>
              {/* // */}

              <Box className='backgoundHover' sx={{ paddingTop: '0.5rem' }}>
                <Chart type='bar' options={expensesOptions} data={expensesData} />
              </Box>

            </Paper>
            {/* /expenses/ */}
          </Grid>
          <Grid item xs={12} md={4}>
            {/* // entries */}
            <Paper elevation={8} sx={{ marginBottom: '2.3rem' }}>
              <Box sx={{ padding: '0.7rem', backgroundColor: '#F4F4F4', color: '#979797', textAlign: 'left' }}>
                <Typography>Entries</Typography>
              </Box>
              {/* // */}
              <Box className='backgoundHover' sx={{ paddingTop: '0.5rem', padding: ".8rem" }}>
                <Chart type='line' options={entriesOptions} data={entriesData} />
              </Box>
            </Paper>
            {/* /entries/ */}
          </Grid>
        </Grid>
      </Box>
      <Navigation />
    </Box>
  )
}