import { Box, Button, Container, Grid, Icon, TextField, Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { useApp } from '../context/appContext';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function WelcomeThree() {
    const { categories } = useApp()
    const { updateCategories } = useApp()
    const navigate = useNavigate();


    function submitFn() {
        updateCategories(categories)
        navigate('/overview')
    }
    return (
        <Paper sx={{ maxWidth: '425px', marginX: 'auto', marginY: '2rem' }} elevation={24}>
            <Box sx={{ paddingX: '1.5rem' }}>
                <Container sx={{ padding: '3rem' }}>
                    <img src={require("../logo/logo-text.png")} className='logoimg' alt='img'/>
                </Container>
                <Typography variant="h4" sx={{ marginBottom: '2.5rem', paddingBottom: '2.5rem' }}>WELCOME</Typography>
                <Typography sx={{ marginBottom: '2.5rem' }}>Set how much money you want to <br></br> spend on each Category monthly</Typography>
                {categories.map((item) => (
                    <Box key={`amount-category-${item.id}`}>
                        <Grid container >
                            <Grid xs={2} item>
                                <Item elevation={0} sx={{ color: 'black' }}><Icon>{item.icon}</Icon></Item>
                            </Grid>
                            <Grid xs={10} item container sx={{ borderBottom: "1px solid gray" }}>
                                <Grid xs={8} item>
                                    <Item elevation={0} sx={{ textAlign: 'left', color: 'black' }}>{item.name}</Item>
                                </Grid>
                                <Grid xs={4} item>
                                    <Item elevation={0} sx={{ padding: '0' }}><TextField variant="outlined" onChange={e => item.budget = +e.target.value} /></Item>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                ))}
                <Button variant="contained" color='secondary' sx={{ width: '100%', marginY: '3rem' }}
                    onClick={submitFn}
                >COMPLETE</Button>
            </Box>
        </Paper>
    )
}