import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { Button, Container, FormControl, IconButton, InputAdornment, InputLabel, OutlinedInput, Paper } from '@mui/material';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';

interface State {
  amount: string;
  password: string;
  weight: string;
  weightRange: string;
  showPassword: boolean;
}

export default function SignIn() {
  const [values, setValues] = React.useState<State>({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });

  const [myEmail, setMyEmail] = React.useState('')
  const [myPassword, setMyPassword] = React.useState('')

  const [emailError, setEmailError] = React.useState<boolean>()
  const [emailErrorMsg, setEmailErrorMsg] = React.useState('')

  const [passError, setPassError] = React.useState<boolean>()
  const [passErrorMsg, setPassErrorMsg] = React.useState('')

  const [user, setUser] = React.useState({})
  const navigate = useNavigate();


  React.useEffect(() => {
    if (Object.keys(user).length !== 0) {
      navigate('/overview')
    }
  }, [user])

  const handleChange =
    (eprop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setValues({ ...values, [eprop]: event.target.value });
      setMyPassword(event.target.value)

      if (myPassword.length > 8 && myPassword.length < 32) {
        setPassError(false)
        setPassErrorMsg('')
      }
      if (myPassword.length < 8) {
        setPassError(true)
        setPassErrorMsg('Password must be at least 8 characters')
      }
      if (myPassword.length > 32) {
        setPassError(true)
        setPassErrorMsg('Password must be less than 32 characters')
      }
    };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const validateEmailFn = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setMyEmail(e.target.value)
    validateEmail(myEmail)
  }

  function onFormSubmit(e: React.FormEvent) {
    e.preventDefault()
    if (passError === false && emailError === false) {
      fetch('https://randomuser.me/api')
        .then(res => res.json())
        .then(data => setUser(data.results[0]))
    }
  }

  function validateEmail(email: string) {
    let reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    if (reg.test(email)) {
      setEmailError(false)
      setEmailErrorMsg('')
    }
    else {
      setEmailError(true)
      setEmailErrorMsg('Invalid Email')
    }
  }
  return (
    <Paper sx={{ maxWidth: '425px', marginX: 'auto', marginY: '2rem' }} elevation={24}>
      <Box sx={{ paddingBottom: '3rem' }}>
        <Container sx={{ padding: '3.5rem' }}>
          <img src={require("../logo/logo-text.png")} className='logoimg' alt='logo'/>
        </Container>
        <Typography variant="h4" sx={{ marginBottom: '2.5rem' }}>SIGN IN</Typography>
        <form onSubmit={onFormSubmit}>
          <Container sx={{ marginBottom: '2rem', paddingX: '2rem', textAlign: 'left' }}>
            <TextField id="outlined-basic" label="Username" size="medium" variant="outlined" sx={{ width: '100%' }}
              onChange={validateEmailFn}
              error={emailError}
            />
            {emailErrorMsg && <Typography variant='caption' sx={{ color: '#d32f2f', marginLeft: '0.9rem' }}>{emailErrorMsg}</Typography>}
          </Container>
          <Container sx={{ marginBottom: '2rem', paddingX: '2rem', textAlign: 'left' }}>
            <FormControl sx={{ width: '100%' }} variant="outlined" error={passError}>
              <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
              <OutlinedInput

                id="outlined-adornment-password"
                type={values.showPassword ? 'text' : 'password'}
                value={values.password}
                onChange={handleChange('password')}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
              />
            </FormControl>
            {passErrorMsg && <Typography variant='caption' sx={{ color: '#d32f2f', marginLeft: '0.9rem' }}>{passErrorMsg}</Typography>}
          </Container>
          <Button type='submit' variant="contained" color='secondary' sx={{ marginTop: '2rem' }}>SIGN IN</Button>
        </form>
        <Box sx={{ marginY: '1rem' }}>
          <small>Don`t have account yet?</small>
        </Box>
        <a href="/singup">Sign up now, it is free!</a>
      </Box>
    </Paper>
  )
} 