import {
  Backdrop,
  Box,
  Dialog, Grid, List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography
} from '@mui/material'
import Icon from '@mui/material/Icon'
import { useState } from 'react'
import AddEditCategory from '../components/AddEditCategory'
import Header from '../components/Header'
import Navigation from '../components/Navigation'
import { useApp } from '../context/appContext'
import { CategoriesType } from '../data/categories'

type Props = {}

export default function Categories({ }: Props) {
  const [addOpen, setAddOpen] = useState(false)
  const [updateOpen, setUpdateOpen] = useState(false)

  const { categories } = useApp()
  const { addCategory } = useApp()

  const [data, setData] = useState<CategoriesType>({
    id: new Date().valueOf(),
    name: '',
    icon: "",
    type: 'income',
    budget: 0,
    spendBuget: 0,
    isEnabled: true
  })
  const newData: CategoriesType = {
    id: new Date().valueOf(),
    name: '',
    icon: "",
    type: 'income',
    budget: 0,
    spendBuget: 0,
    isEnabled: true
  }


  const handleAddClose = () => {
    setAddOpen(false)
  }
  const handleUpdateClose = () => {
    setUpdateOpen(false)
  }

  return (
    <Box>
      <Header heading='Categories' />
      <Box sx={{ padding: '1.5rem', paddingBottom: "100px" }}>
        <Paper elevation={3}>
          <Box sx={{ padding: '0.7rem', backgroundColor: '#F4F4F4', color: '#979797', textAlign: 'left' }}>
            <Typography>Categories</Typography>
          </Box>
          <List>
            <ListItem
              sx={{ paddingX: "0" }}
              onClick={() => {
                setAddOpen(true)
              }}
            >
              <ListItemButton>
                <ListItemIcon>
                  <Icon>add</Icon>
                </ListItemIcon>
                <ListItemText primary={'Add Category'} sx={{ borderBottom: '1px solid #9E9E9E', pb: '5px' }} />
              </ListItemButton>
            </ListItem>

            {categories?.map(category => (
              <ListItem
                onClick={() => {
                  setData({
                    id: category.id,
                    name: category.name,
                    icon: category.icon,
                    type: category.type,
                    budget: category.budget,
                    spendBuget: 0,
                    isEnabled: category.isEnabled
                  })
                  setUpdateOpen(true)
                }}
                key={category.id} sx={{ paddingX: "0", color: category.type === 'income' ? 'green' : 'red' }}>
                <ListItemButton>
                  <ListItemIcon>
                    <Icon
                      sx={{ color: category.type === 'income' ? 'green' : 'red' }}
                      color='primary'
                    >
                      {category.icon}
                    </Icon>
                  </ListItemIcon>
                  <Grid item container sx={{ borderBottom: '1px solid #9E9E9E', pb: '5px' }}>
                    <Grid item xs={9}>
                      <Typography>{category.name}</Typography>
                    </Grid>
                    <Grid item xs={3} sx={{ textAlign: 'right' }}>
                      <Typography>{category.budget}</Typography>
                    </Grid>
                  </Grid>
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Paper>
        <Backdrop
          sx={{
            color: '#fff',
            backgroundColor: 'rgba(255,255,255, .6)'
          }}
          open={addOpen}
        >
          <Dialog open={addOpen} onClose={handleAddClose} hideBackdrop={true}>
            <AddEditCategory
              buttonText='ADD'
              heading='Add New Category'
              handleClose={handleAddClose}
              handleSuccess={handleAddClose}
              addUpdateCategory={addCategory}
              updateData={newData}
            />
          </Dialog>
        </Backdrop>
        <Backdrop
          sx={{
            color: '#fff',
            backgroundColor: 'rgba(255,255,255, .6)'
          }}
          open={updateOpen}
        >
          <Dialog open={updateOpen} onClose={handleUpdateClose} hideBackdrop={true}>
            <AddEditCategory
              buttonText='UPDATE'
              heading='Update Category'
              handleClose={handleUpdateClose}
              handleSuccess={handleUpdateClose}
              addUpdateCategory={addCategory}
              updateData={data}
            />
          </Dialog>
        </Backdrop>
        <Navigation />
      </Box>
    </Box >
  )
}
