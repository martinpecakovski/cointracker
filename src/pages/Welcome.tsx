import { Box, Button, Container, Paper, TextField, Typography } from '@mui/material';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';


export default function Welcome() {
  const navigate = useNavigate();
  const [userBuget, setUserBuget] = useState(0)

  function formSubmit(e: React.FormEvent) {
    e.preventDefault()
    navigate('/welcomeTwo')
  }

  return (
    <Paper sx={{ maxWidth: '425px', marginX: 'auto', marginY: '2rem' }} elevation={24}>
      <Box sx={{ paddingX: '1.5rem' }}>
        <Container sx={{ padding: '3.5rem' }}>
          <img src={require("../logo/logo-text.png")} className='logoimg' alt='logo' />
        </Container>
        <Typography variant="h4" sx={{ marginBottom: '2.5rem' }}>WELCOME</Typography>
        <Typography>How much money you have at the moment?</Typography>
        <form>
          <TextField color='secondary' label="Amount" variant="filled" sx={{ width: '100%', marginY: '3rem' }}
            onChange={(e) => setUserBuget(+e.target.value)}
            error={userBuget <= 0 ? true : false}
            helperText={userBuget <= 0 ? "Amount has to be greater than 0" : ''}
          />
          <Button onClick={formSubmit} variant="contained" disabled={userBuget! <= 0 ? true : false} color='secondary' sx={{ width: '100%', marginY: '3rem' }}>ADD</Button>
        </form>
      </Box>
    </Paper>
  )
}