import { Box, Button, Checkbox, Container, Grid, Icon, Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { useApp } from '../context/appContext';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function WelcomeTwo() {
    const { categories } = useApp()
    const { updateCategories } = useApp()
    const navigate = useNavigate();


    function updateData() {
        let filteredCategories = categories.filter(item => item.isEnabled === true)
        updateCategories(filteredCategories)
        navigate('/welcomeThree')
    }

    return (
        <Paper sx={{ maxWidth: '425px', marginX: 'auto', marginY: '2rem' }} elevation={24}>
            <Box sx={{ paddingX: '1.5rem' }}>
                <Container sx={{ padding: '3.5rem' }}>
                    <img src={require("../logo/logo-text.png")} className='logoimg' alt='logo'/>
                </Container>
                <Typography variant="h4" sx={{ marginBottom: '2.5rem' }}>WELCOME</Typography>
                <Typography sx={{ marginBottom: '2.5rem' }}>Choose what you spend money on</Typography>
                {categories.map((item) => (
                    <Box key={`inital-category-${item.id}`} sx={{ mt: '0.7rem' }}>
                        <Grid container >
                            <Grid xs={2} item>
                                <Item elevation={0} sx={{ color: 'black', textAlign: 'left', pl: '0' }}><Icon>{item.icon}</Icon></Item>
                            </Grid>
                            <Grid xs={10} item container sx={{ borderBottom: "1px solid gray" }}>
                                <Grid xs={10} item>
                                    <Item elevation={0} sx={{ textAlign: 'left', color: 'black' }}>{item.name}</Item>
                                </Grid>
                                <Grid xs={2} item>
                                    <Item elevation={0} sx={{ padding: '0' }}><Checkbox defaultChecked={item.isEnabled} onChange={(e) => item.isEnabled = (e.target.checked)} sx={{ color: 'black' }} /></Item>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                ))}
                <Button onClick={updateData} variant="contained" color='secondary' sx={{ width: '100%', marginY: '3rem' }}>DONE</Button>
            </Box>
        </Paper >
    )
}