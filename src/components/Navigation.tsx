import AddIcon from '@mui/icons-material/Add';
import { Box, Fab, Grid, Typography } from '@mui/material';
import Icon from '@mui/material/Icon';
import { useLocation } from 'react-router-dom';

type Props = {
    dialogOpen?: () => void
}

export default function Navigation({ dialogOpen }: Props) {
    let location = useLocation();
    let path = location.pathname
    return (
        <Box sx={{ position: 'fixed', bottom: '0', left: '0', right: '0' }}>
            <Grid container item spacing={1} sx={{ backgroundColor: '#6200EE', alignItems: 'center' }}>
                <Grid item xs={3}>
                    <a href="/overview" className={path === '/overview' ? 'navItem navItemActive' : 'navItem'}>
                        <Icon fontSize='large'>home_icon</Icon>
                        <Typography fontSize={'12px'}>Overview</Typography>
                    </a>
                </Grid>
                <Grid item xs={3}>
                    <a href="/categories" className={path === '/categories' ? 'navItem navItemActive' : 'navItem'}>
                        <Icon fontSize='large'>category_icon</Icon>
                        <Typography fontSize={'12px'}>Categories</Typography>
                    </a>
                </Grid>
                <Grid item xs={3}>
                    <a href="/statistics" className={path === '/statistics' ? 'navItem navItemActive' : 'navItem'}>
                        <Icon fontSize='large'>bar_chart_icon</Icon>
                        <Typography fontSize={'12px'}>Statistics</Typography>
                    </a>
                </Grid>
                <Grid item xs={3} sx={{ postion: 'relative' }}>
                    <Fab color="primary" aria-label="add" sx={{ postion: 'absolute', top: '-38px' }}>
                        <AddIcon onClick={dialogOpen} />
                    </Fab>
                </Grid>
            </Grid>
        </Box>
    )
}