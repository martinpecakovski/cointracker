import { Grid, Icon, Paper, Typography } from '@mui/material';
import Fab from '@mui/material/Fab';
import { styled } from '@mui/material/styles';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

type Props = {
    heading: string
}

export default function Header({ heading }: Props) {
    return (
        <Grid container spacing={1} sx={{ backgroundColor: '#6200EE', alignItems: 'center' }}>
            <Grid xs={3} item>
                <Item elevation={0} sx={{ backgroundColor: '#6200EE', color: '#FFF', maxHeight: '70px' }}>
                    <img className='maxHeight70' src={require("../logo/logo.png")} alt='logo'/>
                </Item>
            </Grid>
            <Grid xs={6} item>
                <Item elevation={0} sx={{ backgroundColor: '#6200EE', color: '#FFF', textAlign: 'left' }}>
                    <Typography variant='h6'>{heading}</Typography>
                </Item>
            </Grid>
            <Grid xs={3} item>
                <Item elevation={0} sx={{ backgroundColor: '#6200EE', color: '#FFF' }}>
                    <Fab color="inherit" aria-label="edit" sx={{ border: '1px solid #FFF' }}>
                        <Icon fontSize='large'>pie_chart_two_tone_icon</Icon>
                    </Fab>
                </Item>
            </Grid>
        </Grid>
    )
}