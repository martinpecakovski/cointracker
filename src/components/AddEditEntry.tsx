import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel,
    MenuItem,
    Select
} from '@mui/material';
import TextField from '@mui/material/TextField';
import { useState } from 'react';
import { useApp } from '../context/appContext';
import { CategoryType, EntriesType } from '../data/categories';
type Props = {
    handleSuccess: () => void
    handleClose: () => void
    addUpdateEntry: (data: EntriesType) => void
    buttonText: string
    heading: string
    updateData?: EntriesType

}

export default function AddEditEntry({
    handleSuccess,
    handleClose,
    addUpdateEntry,
    buttonText,
    heading,
    updateData
}: Props) {
    const [data, setData] = useState<EntriesType>(updateData!)
    const { categories } = useApp()

    const fillteredIncome = categories.filter(category => category.type === 'income')
    const fillteredExpense = categories.filter(category => category.type === 'expense')

    return (
        <>
            <DialogTitle>{heading}</DialogTitle>
            <form
                onSubmit={e => {
                    e.preventDefault()
                    addUpdateEntry(data)
                }}
            >
                <DialogContent>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <InputLabel id='type'>Type</InputLabel>
                        <Select
                            labelId='type'
                            label='Age'
                            value={data.type}
                            onChange={e =>
                                setData({ ...data, type: e.target.value as CategoryType })

                            }
                        >
                            <MenuItem value={'income'}>Income</MenuItem>
                            <MenuItem value={'expense'}>Expense</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <InputLabel id='type'>Category</InputLabel>
                        <Select
                            labelId='type'
                            label='Age'
                            value={data.category}
                            onChange={e =>
                                setData({ ...data, category: e.target.value })
                            }
                        >
                            {data.type === 'income' ?
                                fillteredIncome.map(category => (
                                    <MenuItem key={`menu-item-${category.id}`} value={category.name}>{category.name}</MenuItem>
                                )) :
                                fillteredExpense.map(category => (
                                    <MenuItem key={`menu-item-${category.id}`} value={category.name}>{category.name}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <TextField
                            type={'number'}
                            label='Amount'
                            variant='outlined'
                            value={data.amount}
                            onChange={e =>
                                setData({ ...data, amount: +e.target.value })
                            }
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <input type="date" className='datePicker'
                            value={data.date}
                            onChange={e =>
                                setData({ ...data, date: e.target.value })
                            }
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <TextField id="outlined-basic" label="Description (otional)" variant="outlined"
                            value={data.desc}
                            onChange={e =>
                                setData({ ...data, desc: e.target.value })
                            }
                        />
                    </FormControl>

                </DialogContent>
                <DialogActions sx={{ justifyContent: 'space-between' }}>
                    <Button color='secondary' onClick={handleClose}>Cancel</Button>
                    <Button variant='contained' color='secondary' type='submit' onClick={handleSuccess}>
                        {buttonText}
                    </Button>
                </DialogActions>
            </form>
        </>
    )
}
