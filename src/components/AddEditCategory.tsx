import {
    Button,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel,
    MenuItem,
    Select
} from '@mui/material'
import Autocomplete from '@mui/material/Autocomplete'
import Checkbox from '@mui/material/Checkbox'
import FormControlLabel from '@mui/material/FormControlLabel'
import TextField from '@mui/material/TextField'
import { useState } from 'react'
import { CategoriesType, CategoryType, initialIcons } from '../data/categories'

type Props = {
    handleSuccess: () => void
    handleClose: () => void
    addUpdateCategory: (data: CategoriesType) => void
    buttonText: string
    heading: string
    updateData?: CategoriesType
}

export default function AddEditCategory({
    handleSuccess,
    handleClose,
    addUpdateCategory,
    buttonText,
    heading,
    updateData
}: Props) {

    const [data, setData] = useState<CategoriesType>(updateData!)
    return (
        <>
            <DialogTitle>{heading}</DialogTitle>
            <form
                onSubmit={e => {
                    e.preventDefault()

                    addUpdateCategory(data)
                }}
            >
                <DialogContent>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <InputLabel id='type'>Type</InputLabel>
                        <Select
                            labelId='type'
                            value={data.type}
                            label='Age'
                            onChange={e =>
                                setData({ ...data, type: e.target.value as CategoryType })
                            }
                        >
                            <MenuItem value={'income'}>Income</MenuItem>
                            <MenuItem value={'expense'}>Expense</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <TextField
                            label='Name'
                            variant='outlined'
                            value={data.name}
                            onChange={e => setData({ ...data, name: e.target.value })}
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <Autocomplete
                            fullWidth
                            disablePortal
                            options={initialIcons}
                            getOptionLabel={c => c.name}
                            renderInput={params => <TextField {...params} label='Icon' />}
                            onChange={(e, newVal) =>
                                setData({ ...data, icon: newVal?.icon || '' })
                            }
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <TextField
                            value={data.budget}
                            onChange={e => setData({ ...data, budget: +e.target.value })}
                            type={'number'}
                            label='Budget'
                            variant='outlined'
                        />
                    </FormControl>

                    <FormControl fullWidth sx={{ pb: '.7rem' }}>
                        <FormControlLabel
                            value={data.isEnabled}
                            control={<Checkbox defaultChecked />}
                            label='Enabled'
                        />
                    </FormControl>
                </DialogContent>
                <DialogActions sx={{ justifyContent: 'space-between' }}>
                    <Button color='secondary' onClick={handleClose}>Cancel</Button>
                    <Button variant='contained' color='secondary' type='submit' onClick={handleSuccess}>
                        {buttonText}
                    </Button>
                </DialogActions>
            </form>
        </>
    )
}
